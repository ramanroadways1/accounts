<?php
require_once("connect.php");

$timestamp = date("Y:m:d H:i:s");

$company = escapeString($conn,strtoupper($_POST['company']));

$update = Qry($conn,"UPDATE rtgs_fm SET colset='1' WHERE fm_date>='2018-04-01' AND colset_d!='1' AND com='$company' AND approval='1'");
	
if(!$update)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

echo "<script>LoadTable();$('#loadicon').fadeOut('slow');</script>";
?>