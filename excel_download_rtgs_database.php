<?php 
require_once './connect.php';

$from = escapeString($conn,$_POST['from_date']);
$to = escapeString($conn,$_POST['to_date']);

$date2 = date("Y-m-d"); 
$time_1=date('H:i:s');
$timestamp=$date2." ".$time_1;

$output = '';

$query = Qry($conn,"SELECT fno,com,acno,acname,amount,type,pay_date,branch,pan,crn,bank,utr_date,timestamp_approve,time_download 
FROM rtgs_fm WHERE pay_date BETWEEN '$from' AND '$to' ORDER BY id ASC");

if(!$query)
{
	echo mysqli_error($conn);
	exit();
}

if(numRows($query) == 0)
{
	echo "<script>
		alert('No record found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
   <table border="1">  
                    <tr>  
                         <th>Vou No</th>  
                         <th>Company</th>  
                         <th>A/c No.</th>  
                         <th>A/c Holder</th>  
                         <th>Amount</th>  
                         <th>Vou Type</th>  
                         <th>Payment Date</th>  
                         <th>Branch</th>  
                         <th>PAN No</th>  
                         <th>Ref No</th>  
                         <th>Approval_Timestamp</th>  
                         <th>Download_Timestamp</th>  
                         <th>UTR No</th>  
                         <th>UTR Date</th>  
					</tr>
  ';
  while($row = fetchArray($query))
  {
   $output .= '
    <tr>  
							<td>'.$row["fno"].'</td>  
							<td>'.$row["com"].'</td>  
							<td>'."'".$row["acno"].'</td>  
							<td>'.$row["acname"].'</td>  
							<td>'.$row["amount"].'</td>  
							<td>'.$row["type"].'</td>  
						   <td>'.$row["pay_date"].'</td>
						   <td>'.$row["branch"].'</td>
						   <td>'.$row["pan"].'</td>
						   <td>'.$row["crn"].'</td>
						   <td>'.$row["timestamp_approve"].'</td>
						   <td>'.$row["time_download"].'</td>
						   <td>'.$row["bank"].'</td>
						   <td>'.$row["utr_date"].'</td>
    </tr>
   ';
  }
	
	$output .= '</table>';
	header('Content-Type: application/xls');
	header('Content-Disposition: attachment; filename=DATABASE_NEFT_'.$from."-".$to.'.xls');
	echo $output;
	closeConnection($conn);
?>