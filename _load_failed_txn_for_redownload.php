<?php
require_once("connect.php");

?>
	<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                       <th>Branch</th>
						<th>Company</th>
						<th>Vou_No</th>
						<th>Ac_Holder</th>
						<th>Ac_No</th>
						<th>Amount</th>
						<th>Payment_Date</th>
						<th>CRN</th>
						<th>#Redownload</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT f.id,f.fno,f.com as company,main.acno,main.acname,f.amount,
		main.pay_date,f.branch,f.crn FROM rtgs_failed as f 
		LEFT JOIN rtgs_fm as main ON main.crn=f.crn
		WHERE f.timestamp>=(now() - interval 6 month)");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='11'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$pay_date = date("d-m-y",strtotime($row['pay_date']));
			
		echo "<tr style='font-size:13px !important'>
				<td>$i</td>
				<td>$row[branch]</td>
				<td>$row[company]</td>
				<td>$row[fno]</td>
				<td>$row[acname]</td>
				<td>$row[acno]</td>
				<td>$row[amount]</td>
				<td>$pay_date</td>
				<td>$row[crn]</td>
				<td><button type='button' class='btn btn-xs btn-danger' id='btn_$row[id]' onclick='MoveToReDownload($row[id])'><span class='fa fa-download'></span> Re-Download</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
        </tbody>
    </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable({
		"lengthMenu": [ [10, 25, 100, -1], [10, 25, 100, "All"] ], 
	});
} );
</script> 			  