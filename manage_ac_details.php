<?php include("header.php"); ?>

<script>
$(function() {
		$("#vehicle_no").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_market_vehicle.php',
		select: function (event, ui) { 
            $('#vehicle_no').val(ui.item.value);   
            $('#table_id').val(ui.item.oid);   
            $('#ac_holder').val(ui.item.ac_name);   
            $('#ac_no').val(ui.item.ac_no);   
            $('#bank_name').val(ui.item.bank_name);   
            $('#ifsc_code').val(ui.item.ifsc);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#vehicle_no').val("");
			$('#table_id').val("");
			$('#ac_holder').val("");
			$('#ac_no').val("");
			$('#bank_name').val("");
			$('#ifsc_code').val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists !</font>',});
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#broker_name").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_broker.php',
		select: function (event, ui) { 
            $('#broker_name').val(ui.item.value);   
            $('#table_id').val(ui.item.id);   
            $('#ac_holder').val(ui.item.ac_name);   
            $('#ac_no').val(ui.item.ac_no);   
            $('#bank_name').val(ui.item.bank_name);   
            $('#ifsc_code').val(ui.item.ifsc);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#broker_name').val("");
			$('#table_id').val("");
			$('#ac_holder').val("");
			$('#ac_no').val("");
			$('#bank_name').val("");
			$('#ifsc_code').val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Broker does not exists !</font>',});
			// alert('Broker does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#driver_name").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_own_truck_driver.php',
		select: function (event, ui) { 
            $('#driver_name').val(ui.item.value);   
            $('#table_id').val(ui.item.id);   
            $('#ac_holder').val(ui.item.ac_name);   
            $('#ac_no').val(ui.item.ac_no);   
            $('#bank_name').val(ui.item.bank_name);   
            $('#ifsc_code').val(ui.item.ifsc);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#driver_name').val("");
			$('#table_id').val("");
			$('#ac_holder').val("");
			$('#ac_no').val("");
			$('#bank_name').val("");
			$('#ifsc_code').val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Driver does not exists !</font>',});
			// alert('Driver does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#exp_party_pan").autocomplete({
		source: './autofill/get_exp_vou_party.php',
		select: function (event, ui) { 
            $('#exp_party_pan').val(ui.item.value);   
            $('#table_id').val(ui.item.id);   
            $('#ac_holder').val(ui.item.name);   
            $('#ac_no').val(ui.item.acno);   
            $('#bank_name').val(ui.item.bank);   
            $('#ifsc_code').val(ui.item.ifsc);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#exp_party_pan').val("");
			$('#table_id').val("");
			$('#ac_holder').val("");
			$('#ac_no').val("");
			$('#bank_name').val("");
			$('#ifsc_code').val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>PAN does not exists !</font>',});
			// alert('Driver does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:14px;">Manage A/c Details :</font></h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
		
		<div class="form-group col-md-12"></div>
		
		<form id="AcUpdateForm" autocomplete="off">
		
			<div class="row">
				
				<div class="form-group col-md-4">
					
					<script>
					function SearchBy(elem)
					{
						$('#vehicle_no').val('');
						$('#broker_name').val('');
						$('#driver_name').val('');
						$('#exp_party_pan').val('');
						$('#chq_copy').val('');
						
						$('#ac_holder').val('');
						$('#ac_no').val('');
						$('#bank_name').val('');
						$('#ifsc_code').val('');
							
						if(elem=='VEHICLE_NO')
						{
							$('#vehicle_no_div').show();
							$('#broke_name_div').hide();
							$('#driver_name_div').hide();
							$('#exp_party_div').hide();
							
							$('#ac_type').html('Market Vehicle');
							
							$('#vehicle_no').attr('required',true);
							$('#broker_name').attr('required',false);
							$('#driver_name').attr('required',false);
							$('#exp_party_pan').attr('required',false);
							
							$('#chq_copy').attr('required',true);
							$('#chq_copy').attr('disabled',false);
						}
						else if(elem=='BROKER_NAME')
						{
							$('#vehicle_no_div').hide();
							$('#broke_name_div').show();
							$('#driver_name_div').hide();
							$('#exp_party_div').hide();
							
							$('#ac_type').html('Broker');
							
							$('#vehicle_no').attr('required',false);
							$('#broker_name').attr('required',true);
							$('#driver_name').attr('required',false);
							$('#exp_party_pan').attr('required',false);
							
							$('#chq_copy').attr('required',true);
							$('#chq_copy').attr('disabled',false);
						}
						else if(elem=='OWN_TRUCK_DRIVER')
						{
							$('#vehicle_no_div').hide();
							$('#broke_name_div').hide();
							$('#driver_name_div').show();
							$('#exp_party_div').show();
							
							$('#ac_type').html('Own Truck Driver');
							
							$('#vehicle_no').attr('required',false);
							$('#broker_name').attr('required',false);
							$('#exp_party_pan').attr('required',false);
							$('#driver_name').attr('required',true);
							
							$('#chq_copy').attr('required',false);
							$('#chq_copy').attr('disabled',true);
						}
						else if(elem=='EXP_PARTY')
						{
							$('#vehicle_no_div').hide();
							$('#broke_name_div').hide();
							$('#driver_name_div').hide();
							$('#exp_party_div').show();
							
							$('#ac_type').html('Expense Vou. Party');
							
							$('#vehicle_no').attr('required',false);
							$('#broker_name').attr('required',false);
							$('#driver_name').attr('required',false);
							$('#exp_party_pan').attr('required',true);
							
							$('#chq_copy').attr('required',true);
							$('#chq_copy').attr('disabled',false);
						}
						else
						{
							$('#vehicle_no_div').hide();
							$('#broke_name_div').hide();
							$('#driver_name_div').hide();
							$('#exp_party_div').hide();
							
							$('#exp_party_pan').attr('required',true);
							$('#vehicle_no').attr('required',true);
							$('#broker_name').attr('required',true);
							$('#driver_name').attr('required',true);
							
							$('#chq_copy').attr('required',true);
							$('#chq_copy').attr('disabled',true);
						}
					}
					</script>
					
					<div class="form-group col-md-12">
						<label>Search By <font color="red">*</font></label>
						<select onchange="SearchBy(this.value)" name="search_by" id="search_by" class="form-control" required="required">
							<option value="">--select an option--</option>
							<option value="VEHICLE_NO">Market Vehicle Number</option>
							<option value="BROKER_NAME">Broker Name</option>
							<option value="OWN_TRUCK_DRIVER">Own Truck Driver Name</option>
							<option value="EXP_PARTY">Expense Vou. Party</option>
						</select>
					</div>
					
					<div class="form-group col-md-12" id="vehicle_no_div" style="display:none">
						<label>Enter Vehicle Number <font color="red">*</font></label>
						<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" type="text" name="vehicle_no" id="vehicle_no" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-12" id="broke_name_div" style="display:none">
						<label>Enter Broker Name <font color="red">*</font></label>
						<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" type="text" name="broker_name" id="broker_name" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-12" id="driver_name_div" style="display:none">
						<label>Enter Driver Name <font color="red">*</font></label>
						<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" type="text" name="driver_name" id="driver_name" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-12" id="exp_party_div" style="display:none">
						<label>Enter Party's PAN <font color="red">*</font></label>
						<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" name="exp_party_pan" id="exp_party_pan" class="form-control" required="required">
					</div>
						
				</div>
				
				<div class="form-group col-md-8">
					
					<div class="form-group col-md-12">
					</div>
					
					<div style="font-size:13px !important" class="form-group col-md-12">
						Account Type : <span id="ac_type" style="color:maroon"></span>
					</div>
					
					<div class="form-group col-md-6">
						<label>A/c Holder <font color="red">*</font></label>
						<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" id="ac_holder" type="text" name="ac_holder" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-6">
						<label>A/c Number <font color="red">*</font></label>
						<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="ac_no" type="text" name="ac_no" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-4">
						<label>Bank Name <font color="red">*</font></label>
						<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" id="bank_name" type="text" name="bank_name" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-4">
						<label>IFSC Code <font color="red">*</font></label>
						<input maxlength="11" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="ifsc_code" type="text" name="ifsc_code" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-4">
						<label>Attachment <font color="red">*</font></label>
						<input type="file" name="file" id="chq_copy" class="form-control" required="required">
					</div>
					
					<input type="hidden" id="table_id" name="table_id" />
					
					<div class="form-group col-md-6">
						<button type="submit" class="btn btn-success btn-sm" id="submit_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update Account</button>
					</div>
					
				</div>
				
			</div>

			</form>
				
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
 
<script type="text/javascript">
$(document).ready(function (e) {
$("#AcUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_btn").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./save_account_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>


<?php include("footer.php") ?>