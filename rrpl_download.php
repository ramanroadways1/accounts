<?php include("header.php"); ?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:14px;">RTGS Approval : <font color="maroon">RRPL</font></h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
		
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateAcDetails").on('submit',(function(e) {
$("#loadicon").show();
$("#btn_submit_edit_ac").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_ac_details.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#modal_result_div").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<div id="func_result"></div>  

<form target='_blank' id="FormViewVoucher" action='../_view/freight_memo.php' method='POST'>
	<input type='hidden' id='vou_no_id' name='value1'>
	<input type='hidden' value='SEARCH' name='key'>
</form>
 
<script>
function ApproveRejectPayment(id)
{
	// Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter mobile number first !</font>',});
	
	$('#loadicon').show();
		jQuery.ajax({
			url: "select_deselect_payment.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
		error: function() {}
	});
}

function EditRtgs(id)
{
	$('#edit_btn_'+id).attr('disabled',true);
	
	$('#loadicon').show();
		jQuery.ajax({
			url: "_load_modal_for_payment_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
			$("#load_modal_div").html(data);
			},
		error: function() {}
	});
}

function SelectAllPayment(company)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
			$('#loadicon').show();
			jQuery.ajax({
				url: "select_all_payment.php",
				data: 'company=' + company,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
			error: function() {}
		});
	  }
	})
}

function ViewVoucher(vou_no)
{
	$('#vou_no_id').val(vou_no);
	$('#FormViewVoucher')[0].submit();
}

function CancelApproval(id)
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#cancel_approval_btn_'+id).attr('disabled',true);
				$('#loadicon').show();
				jQuery.ajax({
					url: "cancel_approval.php",
					data: 'id=' + id,
					type: "POST",
					success: function(data) {
					$("#func_result").html(data);
					},
				error: function() {}
			});
	  }
	})
}
	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_rtgs_approval.php",
		data: 'company=' + 'RRPL',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({
			iDisplayLength: -1,
			"lengthMenu": [ [10, 25, 100, 500, -1], [10, 25, 100, 500, "All"] ], 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}

LoadTable();
</script>

<button type="button" style="display:none" id="modal_open_btn" data-toggle="modal" data-target="#ModalAcEdit"></button>	

<form id="UpdateAcDetails" autocomplete="off">
<div class="modal fade" id="ModalAcEdit" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl-mini">
      <div class="modal-content" id="load_modal_div" style="">
	  
	  </div>
 </div>
</div>
</form>

<?php include("footer.php") ?>