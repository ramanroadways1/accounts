<?php include("header.php"); ?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:14px;">Re-Download : <font color="maroon">Failed Txns</font></h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
		
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
 
<script>
function MoveToReDownload(id)
{
	if(confirm("Are you sure ?") == true)
	{
		$('#btn_'+id).attr('disabled',true);
		$('#loadicon').show();
			jQuery.ajax({
				url: "move_to_redownload_failed_txn.php",
				data: 'id=' + id,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
			error: function() {}
		});
	}
}

function LoadTable()
{
	jQuery.ajax({
		url: "_load_failed_txn_for_redownload.php",
		data: 'ok=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
			"lengthMenu": [ [10, 25, 100, 500, -1], [10, 25, 100, 500, "All"] ], 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}

LoadTable();
</script>

<?php include("footer.php") ?>