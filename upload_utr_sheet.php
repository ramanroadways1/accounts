<?php include("header.php"); ?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:14px;">UTR Sheet Upload</font></h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
		
		<div class="form-group col-md-12"></div>
		
		<div class="form-group col-md-12">Format : (CRN,UTR_No,UTR_Date)</div>
	
	<div class="form-group col-md-12"></div>
		
				<div class="row">
				
					<div class="form-group col-md-1"></div>
					
					<form id="UtrUploadRRPL">
					<div class="form-group col-md-3">
					
					
						<div class="form-group col-md-12">
							<label>Attach File <font color="red">*</font></label>
							<input type="file" accept=".xlx,.xlsx,.csv" name="file" class="form-control" required="required">
						</div>
						
						<div class="form-group col-md-12">
							<label>Company <font color="red">*</font></label>
							<input type="text" value="RRPL" readonly class="form-control" required="required">
						</div>
						
						<input type="hidden" name="import_key" value="ZYX">
						
						<div class="form-group col-md-12">
							<button id="rrpl_submit" type="submit" class="btn btn-sm btn-success"><i class="fa fa-upload"></i> Upload File</button>
						</div>
					</div>
					</form>
					
					<div class="form-group col-md-3"></div>
					
					<form id="UtrUploadRR">
					<div class="form-group col-md-3">
					
						
						<div class="form-group col-md-12">
							<label>Attach File <font color="red">*</font></label>
							<input type="file" accept=".xlx,.xlsx,.csv" name="file" class="form-control" required="required">
						</div>
						
						<div class="form-group col-md-12">
							<label>Company <font color="red">*</font></label>
							<input type="text" value="RAMAN_ROADWAYS" readonly class="form-control" required="required">
						</div>
						
						<input type="hidden" name="import_key" value="ZYX">
						
						<div class="form-group col-md-12">
							<button id="rr_submit" type="submit" class="btn btn-sm btn-success"><i class="fa fa-upload"></i> Upload File</button>
						</div>
					</div>
					</form>
					
				</div>
				
				
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
 
<script type="text/javascript">
$(document).ready(function (e) {
$("#UtrUploadRRPL").on('submit',(function(e) {
$("#loadicon").show();
$("#rrpl_submit").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./save_utr_upload_rrpl.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UtrUploadRR").on('submit',(function(e) {
$("#loadicon").show();
$("#rr_submit").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./save_utr_upload_rr.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<?php include("footer.php") ?>