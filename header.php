<?php
require_once("./connect.php");
$menu_page_name = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Accounts : Raman Group</title>
	<link rel="shortcut icon" type="image/png" href="favicon_2.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  
  <!-- Morris chart -->
  <link rel="stylesheet" href="../happay/bower_components/morris.js/morris.css">
  <link rel="stylesheet" href="../happay/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../happay/bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="../happay/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../happay/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../happay/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="../happay/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="../happay/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
<link href="google_font.css" rel="stylesheet">

<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
	<script src="https://kit.fontawesome.com/d5ec20b4be.js" crossorigin="anonymous"></script>

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

body {
    overflow-x: auto !important;
}

table{
	font-family:Calibri !important;
}

.selectpicker { width:auto; font-size: 12px !important;}

.ui-autocomplete { z-index:2147483647;font-family: Verdana,Arial,sans-serif; font-size:11px !important;}

::-webkit-scrollbar{
    width: 6px;
	height:6px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

.ui-widget {
  font-family: Verdana !important;
  font-size: 12px !important;
}

input{
	font-size:12px !important;
}

label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}

input[type='text'],input[type='number'],input[type='file'],input[type='date'],input[type='date'],input[type='file'],select,textarea{
	font-size:12px !important;
}
select>option,option{
	font-size:12px !important;
}

.sidebar-menu>li>a{
	cursor:pointer;
}

.treeview>ul>li>a{
	font-family:Verdana !important;
	font-size:12px !important;
	cursor:pointer;
}

.fa-circle-o{
	font-size:10px !important
}
</style>

<script>
var message="Function Disabled!";
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}
function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}
document.oncontextmenu=new Function("return false")

function disableCtrlKeyCombination(e)
{
        //list all CTRL + key combinations you want to disable
        var forbiddenKeys = new Array('a', 'c', 'x', 'v');
        var key;
        var isCtrl;
        if(window.event)
        {
                key = window.event.keyCode;     //IE
                if(window.event.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        else
        {
                key = e.which;     //firefox
                if(e.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        //if ctrl is pressed check if other key is in forbidenKeys array
        if(isCtrl)
        {
                for(i=0; i<forbiddenKeys.length; i++)
                {
                        //case-insensitive comparation
                        if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                        {
                                // alert('Key combination CTRL + ' +String.fromCharCode(key)+' has been disabled.');
                                return false;
                        }
                }
        }
        return true;
}
</script>

</head>

<style type="text/css">
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
 /* background: url(load_icon.gif) center no-repeat #fff; */
  background: url(loading_truck.gif) center no-repeat #fff;
}
</style>

<script type="text/javascript">
$(window).load(function() {
   $(".se-pre-con").fadeOut("slow");;
});
</script>

<script>
function LogoutFunc1()
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		window.location.href='https://rrpl.online/34geXmnqK8pxJJN_RTGS/logout.php';
	  }
	})
}

function CallUrl(url)
{
	$('#loadicon').show();
	if(url!='')
	{
		window.location.href=url;
	}
}
</script>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="loading_truck.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div> 

<!--<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);" class="hold-transition skin-blue sidebar-mini">-->
<body style="font-family: 'Open Sans', sans-serif !important" class="hold-transition skin-blue sidebar-mini">
 
 <div class="se-pre-con"></div>
<div class="wrapper">

  <header class="main-header" style="font-size:12px !important;">

	<a href="./" class="logo" style="background:#FFF">
      <span class="logo-mini"><img src="logo_small_rrpl.png" style="width:100%;height:50px" class="" /></span>
      <span class="logo-lg" id="logo_desktop"><img src="logo_raman_roadways.png" style="margin-top:5px;width:100%;height:40px" class="img-responsive" /></span>
      <span class="logo-lg" id="logo_mobile"><center><img src="logo_raman_roadways.png" style="margin-top:5px;width:50%;height:40px" class="img-responsive" /></center></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		
         <li class="user-menu">
            <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
              <img src="avtar2.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo  "Accounts Department"; ?></span>
				&nbsp; &nbsp; <button onclick="LogoutFunc1()" type="button" 
				class="btn btn-danger btn-xs"><i class="fa fa-power-off"></i> </button></a>
			</a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--
		  <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>

    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
       
      </div>
      
	  <ul style="font-size:12px !important;" class="sidebar-menu" data-widget="tree">          
             
 	<li class="<?php if($menu_page_name=="index_main.php") {echo "active";} ?>">
        <a onclick="CallUrl('./index_main.php')"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
     </li>
	 
	
	 <li class="treeview <?php if($menu_page_name=="rrpl_download.php" || $menu_page_name=="rr_download.php") {echo "active";} ?>">
		<a><i class="fa fa-download"></i> <span>Download Payments </span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
			<li class="<?php if($menu_page_name=="rrpl_download.php") {echo "active";} ?>"><a onclick="CallUrl('./rrpl_download.php')"><i class="fa fa-circle-o"></i> RRPL</a></li>
			<li class="<?php if($menu_page_name=="rr_download.php") {echo "active";} ?>"><a onclick="CallUrl('./rr_download.php')"><i class="fa fa-circle-o"></i> RAMAN_ROADWAYS</a></li>
		</ul>
     </li>
	 
	 <li class="<?php if($menu_page_name=="upload_utr_sheet.php") {echo "active";} ?>">
        <a onclick="CallUrl('./upload_utr_sheet.php')"><i class="fa fa-upload"></i> <span>Upload UTR Sheet</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="manage_ac_details.php") {echo "active";} ?>">
        <a onclick="CallUrl('./manage_ac_details.php')"><i class="fa fa-pencil-square-o"></i> <span>Manage A/c details</span></a>
     </li>
	 
	  <li class="<?php if($menu_page_name=="re_download_failed_txn.php") {echo "active";} ?>">
        <a onclick="CallUrl('./re_download_failed_txn.php')"><i class="fa fa-refresh"></i> <span>Redownload Failed Txn</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="re_download_invalid_utr_txn.php") {echo "active";} ?>">
        <a onclick="CallUrl('./re_download_invalid_utr_txn.php')"><i class="fa fa-refresh"></i> <span>Redownload Invalid UTR</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="rtgs_database.php") {echo "active";} ?>">
        <a onclick="CallUrl('./rtgs_database.php')"><i class="fa fa-database"></i> <span>Payments Database</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="utr_sheet_database.php") {echo "active";} ?>">
        <a onclick="CallUrl('./utr_sheet_database.php')"><i class="fa fa-database"></i> <span>UTR Sheet Database</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="clear_manual_payment.php") {echo "active";} ?>">
		<a onclick="CallUrl('./clear_manual_payment.php')"> <i class="fa fa-envelope-open-o"></i> <span>Clear Manual Payment</span> </a>
     </li>
	 
	<li>
		<a onclick="LogoutFunc1()"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a>
     </li>
		
      </ul>
    </section>
  </aside>
  
  