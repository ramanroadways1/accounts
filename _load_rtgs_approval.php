<?php
require_once("connect.php");

$company = escapeString($conn,strtoupper($_POST['company']));
?>
<form action="./rtgs_download.php" target="_blank" method="POST">
	<input type="hidden" name="company" value="<?php echo $company; ?>">
	<button class="btn btn-xs btn-primary" type="submit"><span class="fa fa-download"></span> Download Excel</button>
	<button type="button" class="pull-right btn btn-xs btn-default" onclick="SelectAllPayment('<?php echo $company; ?>')"><span class="fa fa-check-square-o"></span> Select ALL</button>
</form>
<br />
	<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vou_No</th>
                        <th>Vehicle_No</th>
                        <th>Ac_Holder</th>
                        <th>Ac_No</th>
                        <th>IFSC</th>
                        <th>PAN</th>
                        <th>Vou_Type</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>#Select</th>
                        <th>#Edit</th>
						<th>#Cancel</th>
						<th>CRN</th>
						<th>Approved_At</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT id,fno,acname,branch,acno,bank_name as bank,ifsc,amount,type,pay_date,pan,tno,lrno,colset,approval,crn,
	timestamp_approve FROM rtgs_fm 
	WHERE colset_d!='1' AND com='$company' AND amount>0 AND approval='1' AND fm_date>='2018-04-01' ORDER BY crn ASC");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='15'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp_approve = date("d-m-y h:i A",strtotime($row['timestamp_approve']));
			$req_date = date("d-m-y",strtotime($row['pay_date']));
			
			if($row['colset']=="1")
			{
				$checkbox = "<input id='checkbox_id_$row[id]' onclick='ApproveRejectPayment($row[id])' type='checkbox' checked='checked' />";
			}
			else
			{
				$checkbox = "<input id='checkbox_id_$row[id]' onclick='ApproveRejectPayment($row[id])' type='checkbox' />";
			}
			
			echo "<tr style='font-size:13px !important'>
				<td>$i</td>
				<td style='color:maroon;cursor:pointer;' onclick=ViewVoucher('$row[fno]')>$row[fno]</td>
				<td>$row[tno]</td>
				<td>$row[acname]</td>
				<td>$row[acno]</td>
				<td>$row[ifsc]</td>
				<td>$row[pan]</td>
				<td>$row[type]</td>
				<td>$row[amount]</td>
				<td>$req_date</td>
				<td>$checkbox</td>
				<td><button type='button' class='btn btn-xs btn-default' id='edit_btn_$row[id]' onclick='EditRtgs($row[id])'>Edit</button></td>
				<td><button type='button' class='btn btn-xs btn-danger' id='cancel_approval_btn_$row[id]' onclick='CancelApproval($row[id])'>Cancel</button></td>
				<td>$row[crn]</td>
				<td>$timestamp_approve</td>
			</tr>";
		$i++;	
		}
	}
	?>	
        </tbody>
    </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable({
		"lengthMenu": [ [10, 25, 100, -1], [10, 25, 100, "All"] ], 
	});
} );
</script> 			  