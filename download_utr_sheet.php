<?php 
require_once './connect.php';

$sheet_type = escapeString($conn,$_POST['sheet_type']);
$timestamp = escapeString($conn,$_POST['timestamp']);

$output = '';

$query = Qry($conn,"SELECT crn,utr,utr_date FROM utr_sheet_cache_bak WHERE timestamp='$timestamp' AND sheet_type='$sheet_type'");

if(!$query)
{
	echo mysqli_error($conn);
	exit();
}

if(numRows($query) == 0)
{
	echo "<script>
		alert('No record found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
   <table border="1">  
                    <tr>  
                         <th>CRN</th>  
                         <th>UTR_No</th>  
                         <th>UTR_Date</th>  
                	</tr>
  ';
  while($row = fetchArray($query))
  {
		$output .= '
			<tr>  
				<td>'.$row["crn"].'</td>  
				<td>'."'".$row["utr"].'</td>  
				<td>'.$row["utr_date"].'</td>  
			</tr>';
  }
	
	$output .= '</table>';
	header('Content-Type: application/xls');
	header('Content-Disposition: attachment; filename=UTR_SHEET_'.$sheet_type.'_'.$timestamp.'.xls');
	echo $output;
	closeConnection($conn);
?>