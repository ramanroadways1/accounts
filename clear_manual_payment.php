<?php include("header.php"); ?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:14px;">Clear : <font color="maroon">Manual Payment</font></h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
		
		<div class="form-group col-md-12"></div>
		
			<div class="row">
				
				<div class="form-group col-md-4">
					
					<div class="form-group col-md-12">
						<label>Enter CRN <font color="red">*</font></label>
						<input type="text" name="crn_no" id="crn_no" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-12">
						<button type="button" onclick="Search()" class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
					</div>
					
				</div>
			
			</div>

			<div class="col-md-12 table-responsive" id="load_table">

			</form>
				
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
 
<script>
function SaveUTR(id)
{
	var utr_no = $('#utr_no_'+id).val();
	var utr_date = $('#utr_date_'+id).val();
	
	if(utr_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter UTR number first !</font>',});
	}
	else if(utr_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter UTR date first !</font>',});
	}
	else
	{
		$('#btn_'+id).attr('disabled',true);
		
		$('#loadicon').show();
			jQuery.ajax({
				url: "save_clear_manual_payment.php",
				data: 'id=' + id + '&utr_no=' + utr_no + '&utr_date=' + utr_date,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
			error: function() {}
		});
	}
}

function Search()
{
	var crn = $('#crn_no').val();
	
	if(crn=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter CRN first !</font>',});
	}
	else
	{
		$('#loadicon').show();
		jQuery.ajax({
			url: "_file_search_by_crn_for_manual_payment.php",
			data: 'crn=' + crn,
			type: "POST",
			success: function(data) {
				$("#load_table").html(data);
				$('#example').DataTable({ 
				"lengthMenu": [ [10, 25, 100, 500, -1], [10, 25, 100, 500, "All"] ], 
					"destroy": true, //use for reinitialize datatable
				});
			},
			error: function() {}
		});
	}
}
</script>

<?php include("footer.php") ?>