<?php
include "./connect.php";

$type = escapeString($conn,$_POST['type_load']);
$timestamp = escapeString($conn,$_POST['timestamp']);
$date = date("Y-m-d");

if($type=='SUCCESS')
{
	$sql ="SELECT f.id,f.fno,f.amount,f.acname,f.acno,f.pay_date,f.fm_date,f.type,f.crn,f.bank,f.utr_date 
	FROM utr_sheet_cache_bak AS e 
	LEFT OUTER JOIN	rtgs_fm AS f ON f.crn = e.crn 
	WHERE e.timestamp='$timestamp' AND e.utr!=''";
}
else if($type=='FAILED')
{
	$sql ="SELECT f.id,f.fno,f.amount,f.acname,f.acno,f.pay_date,f.fm_date,f.type,f.crn,f.bank,f.utr_date 
	FROM rtgs_failed AS e 
	LEFT OUTER JOIN	rtgs_fm AS f ON f.crn = e.crn 
	WHERE e.timestamp='$timestamp'";
}
else
{
	$sql ="SELECT f.id,f.fno,f.amount,f.acname,f.acno,f.pay_date,f.fm_date,f.type,f.crn,f.bank,f.utr_date 
	FROM rtgs_failed AS e 
	LEFT OUTER JOIN	rtgs_fm AS f ON f.crn = e.crn 
	WHERE e.timestamp='$timestamp'";
}
			
$table = "(
    ".$sql."
) temp";
  
  
$primaryKey = 'id';
 
if($type=='SUCCESS')
{
	$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'fno', 'dt' => 1),
    array( 'db' => 'amount', 'dt' => 2),
    array( 'db' => 'acname', 'dt' => 3),
    array( 'db' => 'acno', 'dt' => 4),
    array( 'db' => 'pay_date', 'dt' => 5),
    array( 'db' => 'fm_date', 'dt' => 6),
    array( 'db' => 'type', 'dt' => 7),
    array( 'db' => 'crn', 'dt' => 8),
    array( 'db' => 'bank', 'dt' => 9),
    array( 'db' => 'utr_date', 'dt' => 10),
	);
}
else if($type=='FAILED')
{
	$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'fno', 'dt' => 1),
    array( 'db' => 'amount', 'dt' => 2),
    array( 'db' => 'acname', 'dt' => 3),
    array( 'db' => 'acno', 'dt' => 4),
    array( 'db' => 'pay_date', 'dt' => 5),
    array( 'db' => 'fm_date', 'dt' => 6),
    array( 'db' => 'type', 'dt' => 7),
    array( 'db' => 'crn', 'dt' => 8),
    );
}
else
{
	$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'fno', 'dt' => 1),
    array( 'db' => 'amount', 'dt' => 2),
    array( 'db' => 'acname', 'dt' => 3),
    array( 'db' => 'acno', 'dt' => 4),
    array( 'db' => 'pay_date', 'dt' => 5),
    array( 'db' => 'fm_date', 'dt' => 6),
    array( 'db' => 'type', 'dt' => 7),
    array( 'db' => 'crn', 'dt' => 8),
    );
}

 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require( '../b5aY6EZzK52NA8F/scripts/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
);

?>