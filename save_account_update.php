<?php
require_once("connect.php");

$timestamp = date("Y:m:d H:i:s");

$search_by = escapeString($conn,($_POST['search_by']));
$table_id = escapeString($conn,strtoupper($_POST['table_id']));
$ac_holder = escapeString($conn,strtoupper($_POST['ac_holder']));
$ac_no = escapeString($conn,strtoupper($_POST['ac_no']));
$bank_name = escapeString($conn,strtoupper($_POST['bank_name']));
$ifsc_code = escapeString($conn,strtoupper($_POST['ifsc_code']));

if($search_by=='VEHICLE_NO')
{
	$ac_type="OWNER";
	
	$sourcePath = $_FILES['file']['tmp_name'];
	
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

	if(!in_array($_FILES['file']['type'], $valid_types))
	{
		AlertRightCornerError("Only Image Upload Allowed !");
		echo "<script>$('#submit_btn').attr('disabled',false);</script>";
		exit();
	}
	
	$fetch_ac = Qry($conn,"SELECT acname,accno as acno,bank,ifsc FROM mk_truck WHERE id='$table_id'");
}
else if($search_by=='BROKER_NAME')
{
	$sourcePath = $_FILES['file']['tmp_name'];
	
	$ac_type="BROKER";
	
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

	if(!in_array($_FILES['file']['type'], $valid_types))
	{
		AlertRightCornerError("Only Image Upload Allowed !");
		echo "<script>$('#submit_btn').attr('disabled',false);</script>";
		exit();
	}
	
	$fetch_ac = Qry($conn,"SELECT acname,accno as acno,bank,ifsc FROM mk_broker WHERE id='$table_id'");
}
else if($search_by=='EXP_PARTY')
{
	$sourcePath = $_FILES['file']['tmp_name'];
	
	$ac_type="EXP_VOU_PARTY";
	
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

	if(!in_array($_FILES['file']['type'], $valid_types))
	{
		AlertRightCornerError("Only Image Upload Allowed !");
		echo "<script>$('#submit_btn').attr('disabled',false);</script>";
		exit();
	}
	
	$fetch_ac = Qry($conn,"SELECT name as acname,acno,bank,ifsc FROM exp_ac WHERE id='$table_id'");
}
else if($search_by=='OWN_TRUCK_DRIVER')
{
	$ac_type="DRIVER";
	
	$fetch_ac = Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$table_id'");
}
else
{
	AlertRightCornerError("Something went wrong !");
	echo "<script>$('#submit_btn').attr('disabled',false);</script>";
	exit();
}

if(!$fetch_ac)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

if(numRows($fetch_ac) == 0)
{
	AlertRightCornerError("Account details not found !");
	exit();
}

$row_ac = fetchArray($fetch_ac);

$old_acno = $row_ac['acno'];
$old_ifsc = $row_ac['ifsc'];

$old_ac_details = "Ac_Holder: $row_ac[acname], Ac_No: $row_ac[acno], Bank: $row_ac[bank], IFSC: $row_ac[ifsc]";

if($row_ac['acname']==$ac_holder AND $row_ac['acno']==$ac_no AND $row_ac['bank']==$bank_name AND $row_ac['ifsc']==$ifsc_code)
{
	AlertRightCornerError("Nothing to update !");
	exit();
}

StartCommit($conn);
$flag = true;

if($search_by=='OWN_TRUCK_DRIVER')
{
	$update_ac = Qry($conn,"UPDATE dairy.driver_ac SET acname='$ac_holder',acno='$ac_no',bank='$bank_name',ifsc='$ifsc_code' WHERE code='$table_id'");
		
	if(!$update_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insertLog = Qry($conn,"INSERT INTO ac_update (o_b,ac_for,old_ac_details,acname,acno,bank,ifsc,timestamp) VALUES 
	('$table_id','$ac_type','$old_ac_details','$ac_holder','$ac_no','$bank_name','$ifsc_code','$timestamp')");
		
	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($search_by=='VEHICLE_NO')
{
	$update_ac = Qry($conn,"UPDATE mk_truck SET acname='$ac_holder',accno='$ac_no',bank='$bank_name',ifsc='$ifsc_code' WHERE id='$table_id'");
		
	if(!$update_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$fix_name = mt_rand().date('dmYHis');

	$targetPath = "copy/".$fix_name.".".pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
	
	ImageUpload(1000,1000,$sourcePath);
	
	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		$flag = false;
		errorLog("File upload failed",$conn,$page_name,__LINE__);
	}
	
	$insertLog = Qry($conn,"INSERT INTO ac_update (o_b,ac_for,old_ac_details,acname,acno,bank,ifsc,copy,timestamp) VALUES 
	('$table_id','$ac_type','$old_ac_details','$ac_holder','$ac_no','$bank_name','$ifsc_code','$targetPath','$timestamp')");
		
	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($search_by=='EXP_PARTY')
{
	$update_ac = Qry($conn,"UPDATE exp_ac SET name='$ac_holder',acno='$ac_no',bank='$bank_name',ifsc='$ifsc_code' WHERE id='$table_id'");
		
	if(!$update_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$fix_name = mt_rand().date('dmYHis');

	$targetPath = "copy/".$fix_name.".".pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
	
	ImageUpload(1000,1000,$sourcePath);
	
	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		$flag = false;
		errorLog("File upload failed",$conn,$page_name,__LINE__);
	}
	
	$insertLog = Qry($conn,"INSERT INTO ac_update (o_b,ac_for,old_ac_details,acname,acno,bank,ifsc,copy,timestamp) VALUES 
	('$table_id','$ac_type','$old_ac_details','$ac_holder','$ac_no','$bank_name','$ifsc_code','$targetPath','$timestamp')");
		
	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$update_ac = Qry($conn,"UPDATE mk_broker SET acname='$ac_holder',accno='$ac_no',bank='$bank_name',ifsc='$ifsc_code' WHERE id='$table_id'");
		
	if(!$update_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$fix_name = mt_rand().date('dmYHis');

	$targetPath = "copy/".$fix_name.".".pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
	
	ImageUpload(1000,1000,$sourcePath);
	
	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		$flag = false;
		errorLog("File upload failed",$conn,$page_name,__LINE__);
	}
	
	$insertLog = Qry($conn,"INSERT INTO ac_update (o_b,ac_for,old_ac_details,acname,acno,bank,ifsc,copy,timestamp) VALUES 
	('$table_id','$ac_type','$old_ac_details','$ac_holder','$ac_no','$bank_name','$ifsc_code','$targetPath','$timestamp')");
		
	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_pending_payments = Qry($conn,"UPDATE rtgs_fm SET acname='$ac_holder',acno='$ac_no',bank_name='$bank_name',ifsc='$ifsc_code' 
WHERE acno='$old_acno' AND ifsc='$old_ifsc' AND colset_d=''");
	
if(!$update_pending_payments){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("OK : Updated !");	
	echo "<script>
		$('#submit_btn').attr('disabled',false);
		$('#search_by').val('');
		SearchBy('');
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !");
	exit();
}
?>