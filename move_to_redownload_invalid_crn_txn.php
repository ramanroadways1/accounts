<?php
require_once("connect.php");

$timestamp = date("Y:m:d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));

if(empty($id))
{
	AlertRightCornerError("Txn ID not found !");
	exit();
}

$get_crn = Qry($conn,"SELECT d.fno,d.crn,f.type,f.bank 
FROM rtgs_db AS d 
LEFT OUTER JOIN rtgs_fm AS f ON f.crn = d.crn 
WHERE d.id='$id'");
	
if(!$get_crn)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

if(numRows($get_crn) == 0)
{
	AlertRightCornerError("Payment not found !");
	exit();
}

$row_crn = fetchArray($get_crn);

$crn = $row_crn['crn'];

if($row_crn['crn']=='')
{
	AlertRightCornerError("Invalid CRN !");
	exit();
}

if($row_crn['bank']=='')
{
	AlertRightCornerError("Error: This is rejected payment !");
	exit();
}

$payment_type = $row_crn['type'];
$vou_no = $row_crn['fno'];

StartCommit($conn);
$flag = true;

$mark_re_download = Qry($conn,"UPDATE rtgs_fm SET colset_d='',bank='',utr_date='',redown='1' WHERE crn='$crn' AND approval='1' AND colset='1' AND colset_d='1' 
AND bank!=''");

if(!$mark_re_download){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn) == 0)
{
	AlertRightCornerError("Something went wrong !");
	exit();
}

$delte_rtgs_done = Qry($conn,"DELETE FROM rtgs_done WHERE crn='$crn'");

if(!$delte_rtgs_done){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delte_rtgs_failed = Qry($conn,"DELETE FROM rtgs_failed WHERE crn='$crn'");

if(!$delte_rtgs_failed){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delte_rtgs_db = Qry($conn,"DELETE FROM rtgs_db WHERE crn='$crn'");

if(!$delte_rtgs_db){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($payment_type=='ADVANCE')
{
	$fm_update = Qry($conn,"UPDATE freight_form SET rtgs_adv='0',colset_adv='',colset_d_adv='',adv_download='' WHERE frno='$vou_no'");

	if(!$fm_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_type=='BALANCE')
{
	$fm_update = Qry($conn,"UPDATE freight_form SET rtgs_bal='0',colset_bal='',colset_d_bal='',bal_download='' WHERE frno='$vou_no'");

	if(!$fm_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("OK : Done !");	
	echo "<script>
		$('#btn_$id').hide();
		$('#btn_$id').attr('disabled',true);
		$('#btn_$id').attr('onclick','');
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !");
	exit();
}
?>