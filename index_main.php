<?php
include "header.php";

$today_date = date("Y-m-d");

$get_rtgs_data = Qry($conn,"SELECT 
(SELECT COUNT(id) FROM rtgs_fm WHERE date(timestamp)='$today_date' AND com='RRPL') as 'rrpl_today_payment',
(SELECT COUNT(id) FROM rtgs_fm WHERE date(timestamp)='$today_date' AND com='RAMAN_ROADWAYS') as 'rr_today_payment',
(SELECT COUNT(id) FROM rtgs_fm WHERE approval='1' AND colset_d!='1' AND com='RRPL') as 'rrpl_pending_download',
(SELECT COUNT(id) FROM rtgs_fm WHERE approval='1' AND colset_d!='1' AND com='RAMAN_ROADWAYS') as 'rr_pending_download',
(SELECT COUNT(id) FROM rtgs_done WHERE date(timestamp)='$today_date' AND com='RRPL') as 'rrpl_approved_today',
(SELECT COUNT(id) FROM rtgs_done WHERE date(timestamp)='$today_date' AND com='RAMAN_ROADWAYS') as 'rr_approved_today',
(SELECT COUNT(id) FROM rtgs_failed WHERE date(timestamp)='$today_date') as 'failed_today',
(SELECT COUNT(id) FROM rtgs_failed) as 'failed_all'
");

if(!$get_rtgs_data){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_data = fetchArray($get_rtgs_data);
?>
<style>
	#span_header{
		font-size:13px !important;
	}
</style>

<div class="content-wrapper">
    <section class="content-header">
      <h1 style="font-size:14px">
		Dashboard
        <small></small>
      </h1>
    </section>

<section class="content">
		
	<div class="row" style="font-size:12px !important;">
		  
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4><?php echo $row_data['rrpl_today_payment']; ?></h4>
				  <p id="span_header">Today's RRPL Payment</p>
				</div>
				<a href="#" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
		 </div> 
	 
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4><?php echo $row_data['rr_today_payment']; ?></h4>
			<p id="span_header">Today's RR Payment</p>
            </div>
            <a href="#" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4><?php echo $row_data['rrpl_pending_download']; ?></h4>
				<p id="span_header">RRPL Pending Download</p>
            </div>
            <a href="#" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4><?php echo $row_data['rr_pending_download']; ?></h4>

              <p id="span_header">RR Pending Download</p>
            </div>
            <a href="#" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		
	</div>
	
	<div class="row" style="font-size:12px !important;">
		  
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4><?php echo $row_data['rrpl_approved_today']; ?></h4>
				  <p id="span_header">RRPL Approved Today</p>
				</div>
				<a href="#" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
		 </div> 
	 
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4><?php echo $row_data['rr_approved_today']; ?></h4>
			<p id="span_header">RR Approved Today</p>
            </div>
            <a href="#" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4><?php echo $row_data['failed_today']; ?></h4>
				<p id="span_header">Failed Today</p>
            </div>
            <a href="#" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4><?php echo $row_data['failed_all']; ?></h4>

              <p id="span_header">Failed ALL</p>
            </div>
            <a href="#" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		
	</div>
	
	<form target='_blank' id="FormViewVoucher" action='../_view/freight_memo.php' method='POST'>
	<div class="row" style="font-size:12px !important;">
		<div class="form-group col-md-3">
			<label>Search Voucher <font color="red"><sup>*</sup></font></label>
			<input type="text" style="text-transform:uppercase" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" class="form-control" required="required" name="value1" />
			<br />
			<input type='hidden' value='SEARCH' name='key'>
			<button type="submit" name="submit" class="btn btn-primary btn-sm"><span class="fa fa-search"></span> Search</button>
		</div>
	</div>
	</form>
	
</section>
 </div>

<?php
include "footer.php";
?>