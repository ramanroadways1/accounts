<?php include("header.php"); ?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:14px;">Excel : <font color="maroon">Rtgs Database</font></h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
		
		<div class="form-group col-md-12"></div>
		
		<form action="./excel_download_rtgs_database.php" target="_blank" method="POST">
			<div class="row">
				
				<div class="form-group col-md-4">
					
					<div class="form-group col-md-12">
						<label>From Date <font color="red">*</font></label>
						<input type="date" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="from_date" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-12">
						<label>To Date <font color="red">*</font></label>
						<input type="date" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="to_date" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-12">
						<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
					</div>
					
				</div>
				
				
			</div>

		</form>
				
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
 
<?php include("footer.php") ?>