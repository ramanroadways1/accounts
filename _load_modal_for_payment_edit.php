<?php
require_once("connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));

$get_payment = Qry($conn,"SELECT fno,acname,acno,bank_name,ifsc,type,colset_d FROM rtgs_fm WHERE id='$id'");

if(!$get_payment){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_payment) == 0)
{
	AlertRightCornerError("Payment not found !");
	exit();
}

$row = fetchArray($get_payment);

if($row['colset_d']!='')
{
	AlertRightCornerError("Payment downloaded already !");
	exit();
}
?>

	  <div style="font-size:13px" class="modal-header bg-primary">
			Update Account details : <?php echo $row['fno']; ?>
      </div>
	  
	<div class="modal-body">
			<div class="row">
			
			<div class="form-group col-md-12">
				<label>Account Holder <sup><font color="red">*</font></sup></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" style="font-size:12px !important" type="text" name="ac_holder" value="<?php echo $row['acname']; ?>" class="form-control" required="required" />
			</div>
			
			<div class="form-group col-md-12">
				<label>Account Number <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-]/,'')" type="text" name="ac_no" value="<?php echo $row['acno']; ?>" class="form-control" required="required" />
			</div>
			
			<div class="form-group col-md-12">
				<label>Bank Name <sup><font color="red">*</font></sup></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" style="font-size:12px !important" type="text" name="bank" value="<?php echo $row['bank_name']; ?>" class="form-control" required="required" />
			</div>
			
			<div class="form-group col-md-12">
				<label>IFSC Code <sup><font color="red">*</font></sup></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" style="font-size:12px !important" type="text" name="ifsc" value="<?php echo $row['ifsc']; ?>" class="form-control" required="required" />
			</div>
			
			<input type="hidden" value="<?php echo $id; ?>" name="id">
			
			<div class="form-group col-md-12" id="modal_result_div"></div>
			
		</div>
	</div> 
     
	<div class="modal-footer">
		<button type="submit" id="btn_submit_edit_ac" class="btn btn-sm btn-success">Update</button>
		<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
	</div>

 
<script> 
$('#modal_open_btn')[0].click();
$('#loadicon').fadeOut('slow');
</script> 