<?php
include "./connect.php";

$timestamp = escapeString($conn,$_REQUEST['timestamp']);
$company = escapeString($conn,$_REQUEST['company']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>RAMAN GROUP</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
	<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
	<script type="text/javascript" src="https://datatables.net/release-datatables/extensions/Scroller/js/dataTables.scroller.js"></script>
	<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link href="../b5aY6EZzK52NA8F/data_table_custom.css" rel="stylesheet" type="text/css" />


<style>
::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
</style>

</head>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="../happay/loader.gif" /></center>
</div>		  

<body style="font-family: 'Open Sans', sans-serif !important" class="hold-transition skin-blue sidebar-mini">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<button type="button" onclick="window.close();" class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-cross"></span> Close window</button>
		</div>
		<div class="col-md-4">
			<center><h5 id="header_text">Successful Txns : <?php echo $company; ?></h5></center>
		</div>
	</div>	
</div>

	
	<div class="form-group col-md-12 table-responsive" id="getPAGEDIV">
		<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
			<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
				 <thead style="" class="thead-light bg-success">
					<tr>
							<th>#Id</th>  
							<th>Vou_No</th>  
							<th>Amount</th>  
							<th>Ac_Holder</th>  
							<th>Ac_No</th>  
							<th>Payment_Date <span class="glyphicon glyphicon-filter"></span></th>  
							<th>Vou_Date <span class="glyphicon glyphicon-filter"></span></th>  
							<th>Payment_Type</th>  
							<th>CRN</th>  
							<th>UTR_No</th>  
							<th>UTR_Date <span class="glyphicon glyphicon-filter"></span></th>  
					</tr>	
				</thead> 
			</table>
		</div>
	</div>
	
</div>
</div>
</body>
</html>

<script type="text/javascript">
function LoadData(type1,timestamp)
{
	
$("#loadicon").show();
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 100, 500, -1], [50, 100, 500, "All"] ], 
		"bProcessing": true,
		"scroller": true,
        "scrollCollapse": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"responsive": true,
		"ordering": true,
		"buttons": [
			"copy", "excel", "print", "colvis",
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[0, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": true,
    "searchable": false 
	},
	 ], 
        "serverSide": true,
		 "ajax": {
            "url": "_load_data_after_sheet_upload.php",
            "type": "POST",
			"data": function ( d ) {
                d.type_load = type1;
                d.timestamp = timestamp;
                // d.custom = $('#myInput').val();
                // etc
            }
		},
		"columns": [
			{"data": "0"},
			{"data": "1"},
			{"data": "2"},
			{"data": "3"},
			{"data": "4"},
			{"data": "5"},
			{"data": "6"},
			{"data": "7"},
			{"data": "8"},
			{"data": "9"},
			{"data": "10"},
		],
		"initComplete": function(settings, json) {
 		$("#loadicon").hide();
 		}
    } );
}

LoadData('SUCCESS','<?php echo $timestamp; ?>');
</script>