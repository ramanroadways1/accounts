<?php include("header.php"); ?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:14px;">UTR Sheet Database :</font></h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
		
			<div class="form-group col-md-12"></div>
		
			<div class="col-md-12">
				<div class="row">
					
					<div class="form-group col-md-3">
						<label>Sheet Upload Date <font color="red"><sup>*</sup></font></label>
						<input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="utr_date" />
					</div>
							
					<div class="form-group col-md-2">
						<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
						<button type="button" style="margin-top:3px" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
					</div>
					
				</div>
			</div>
			
			<div class="col-md-12 table-responsive" style="overflow:auto" id="load_table"></div> 

				</div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  

<script>	
function Search()
{
	var utr_date = $('#utr_date').val();
	
	if(utr_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select date first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_utr_sheets.php",
				data: 'utr_date=' + utr_date,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
} 
</script>
 
<?php include("footer.php") ?>