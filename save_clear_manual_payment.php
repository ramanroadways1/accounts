<?php
require_once("connect.php");

$timestamp = date("Y:m:d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$utr_no = escapeString($conn,strtoupper($_POST['utr_no']));
$utr_date = escapeString($conn,strtoupper($_POST['utr_date']));

if(empty($id))
{
	AlertRightCornerError("Txn ID not found !");
	exit();
}

if(empty($utr_date))
{
	AlertRightCornerError("UTR date not found !");
	exit();
}

if(empty($utr_no))
{
	AlertRightCornerError("UTR number not found !");
	exit();
}

$get_crn = Qry($conn,"SELECT fno,com,branch,type,amount,colset,colset_d,crn,bank FROM rtgs_fm WHERE id='$id'");
	
if(!$get_crn)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

if(numRows($get_crn) == 0)
{
	AlertRightCornerError("Payment not found !");
	exit();
}

$row_crn = fetchArray($get_crn);

if($row_crn['crn']=='')
{
	AlertRightCornerError("Invalid CRN !");
	exit();
}

if($row_crn['colset']!='')
{
	AlertRightCornerError("Error: Payment downloaded already !");
	exit();
}

if($row_crn['bank']!='')
{
	AlertRightCornerError("Error: UTR number already exists !");
	exit();
}

StartCommit($conn);
$flag = true;

$insert_payment = Qry($conn,"INSERT INTO rtgs_done (fno,com,amount,branch,crn,nrr,timestamp) VALUES ('$row_crn[fno]','$row_crn[com]',
'$row_crn[amount]','$row_crn[branch]','$row_crn[crn]','ADV','$timestamp')");
	
if(!$insert_payment){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_payment_db = Qry($conn,"INSERT INTO rtgs_db (fno,type,com,amount,branch,crn,utr,nrr,timestamp) VALUES ('$row_crn[fno]',$row_crn[type],
'$row_crn[com]','$row_crn[amount]','$row_crn[branch]','$row_crn[crn]','$utr_no','ADV_RTGS_CLEARED','$timestamp')");
	
if(!$insert_payment_db){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_utr_no = Qry($conn,"UPDATE rtgs_fm SET colset_d='1',colset='1',approval='1',timestamp_approve='$timestamp',time_download='$timestamp',
bank='$utr_no',utr_date='$utr_date' WHERE id='$id'");

if(!$update_utr_no){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn) == 0)
{
	AlertRightCornerError("Something went wrong !");
	exit();
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("OK : Done !");	
	echo "<script>
		$('#btn_$id').hide();
		$('#btn_$id').attr('disabled',true);
		$('#utr_no_$id').attr('disabled',true);
		$('#utr_date_$id').attr('disabled',true);
		$('#btn_$id').attr('onclick','');
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !");
	exit();
}
?>