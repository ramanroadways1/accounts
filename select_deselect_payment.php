<?php
require_once("connect.php");

$timestamp = date("Y:m:d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));

$get_current_status = Qry($conn,"SELECT colset FROM rtgs_fm WHERE id='$id'");

if(!$get_current_status)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

if(numRows($get_current_status)==0)
{
	AlertRightCornerError("Payment not found !");
	exit();
}

$row_payment = fetchArray($get_current_status);

if($row_payment['colset']=="1")
{
	$set_value="";
}
else
{
	$set_value="1";
}

$update = Qry($conn,"UPDATE rtgs_fm SET colset='$set_value' WHERE id='$id'");
	
if(!$update)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

echo "<script>$('#loadicon').fadeOut('slow');</script>";
?>