<?php
require_once("connect.php");

$crn = escapeString($conn,$_POST['crn']);
$date_today = date("Y-m-d");

?>
	<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
						<th>Branch</th>
						<th>Company</th>
						<th>Vou_No</th>
						<th>Ac_Holder</th>
						<th>Ac_No</th>
						<th>Amount</th>
						<th>Payment_Date</th>
						<th>CRN</th>
						<th>UTR_No</th>
						<th>UTR_Date</th>
						<th>#Update</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT id,branch,com as company,fno,acname,acno,amount,pay_date,crn FROM rtgs_fm WHERE crn='$crn' AND colset!='1'");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='12'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$pay_date = date("d-m-y",strtotime($row['pay_date']));
			
		echo "<tr style='font-size:13px !important'>
				<td>$i</td>
				<td>$row[branch]</td>
				<td>$row[company]</td>
				<td>$row[fno]</td>
				<td>$row[acname]</td>
				<td>$row[acno]</td>
				<td>$row[amount]</td>
				<td>$pay_date</td>
				<td>$row[crn]</td>
				<td><input type='text' style='height:22px !important' class='form-control' id='utr_no_$row[id]'></td>
				<td><input type='date' style='height:22px !important' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' max='$date_today' class='form-control' id='utr_date_$row[id]'></td>
				<td><button type='button' class='btn btn-xs btn-danger' id='btn_$row[id]' onclick='SaveUTR($row[id])'><span class='fa fa-pencil-square-o'></span> Update</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
        </tbody>
    </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable({
		"lengthMenu": [ [10, 25, 100, -1], [10, 25, 100, "All"] ], 
	});
} );

$('#loadicon').fadeOut('slow');
</script> 			  