<?php
require_once './connect.php';

$date2 = date("Y-m-d"); 
$time_1=date('H:i:s');
$timestamp=$date2." ".$time_1;

$newtime = $date2." ".date('H');

if(isset($_POST['import_key']))
{
	$chk_cache  = Qry($conn,"SELECT id FROM utr_sheet_cache");
	
	if(!$chk_cache)
	{
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		AlertRightCornerError("Error while processing request !");
		exit();
	}
	
	if(numRows($chk_cache)>0)
	{
		AlertRightCornerError("UTR cache already exixts !");
		echo "<script>$('#rrpl_submit').attr('disabled',false);</script>";
		exit();
	}
				
	$allowedExtensions = array("csv");
	
	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					
	if(!in_array($ext, $allowedExtensions))
	{
		AlertRightCornerError("Please upload CSV file !");
		echo "<script>$('#rrpl_submit').attr('disabled',false);</script>";
		exit();
	}
				
	$fileName = $_FILES["file"]["tmp_name"];
	
	StartCommit($conn);
	$flag = true;
	
	if ($_FILES["file"]["size"] > 0)
	{
				$file = fopen($fileName, "r");
				
				while (($column = fgetcsv($file, 10000, ",")) !== FALSE)
				{
					if($column[0]!='' AND $column[0]!='Ref No')
					{
						$sqlInsert = Qry($conn,"INSERT into utr_sheet_cache (crn,utr,utr_date,timestamp)
						values ('$column[0]','$column[1]','" . date("Y-m-d",strtotime($column[2])) . "','$timestamp')");
						
						if(!$sqlInsert){
							$flag = false;
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						}
					}
				}
	}
			
	$qry_sel = Qry($conn,"SELECT crn,utr,utr_date FROM utr_sheet_cache WHERE timestamp='$timestamp'");

	if(!$qry_sel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	if(numRows($qry_sel)==0)
	{
		$flag = false;
		errorLog("Empty file uploaded.",$conn,$page_name,__LINE__);
	}

	while($row_sel = fetchArray($qry_sel))
	{
		if(strpos($row_sel['crn'],"RRPL-V") !== false)
		{
			$ch = curl_init('http://vendor.rrpl.online/get_utr.php');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("crn"=> $row_sel['crn'],"utr"=>$row_sel['utr'],"utr_date"=>$row_sel['utr_date'])));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen(json_encode(array("crn"=> $row_sel['crn'],"utr"=>$row_sel['utr'],"utr_date"=>$row_sel['utr_date']))))
			);
			$result = curl_exec($ch);
			if(curl_errno($ch))
			{
				$flag = false;
				errorLog("API Execution failure !",$conn,$page_name,__LINE__);
				// API Execution failure !
			}
			else
			{
				$output = json_decode($result,true);
				curl_close($ch);

				if($output['status']=='error'){
					echo $output['errormsg'];
				}
				else{
					$response = $output['response'];
				}
			}
		}	
	  
		$update_utr_no = Qry($conn,"UPDATE rtgs_fm SET bank='$row_sel[utr]',utr_date='$row_sel[utr_date]',timestamp_upload='$timestamp' 
		WHERE crn='$row_sel[crn]' AND colset_d='1'");

		if(!$update_utr_no){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	$insert_failed_txn = Qry($conn,"INSERT INTO rtgs_failed(fno,com,amount,branch,crn,timestamp) 
	SELECT fno,com,amount,branch,crn,'$timestamp' FROM rtgs_fm WHERE timestamp_upload='$timestamp' AND CHAR_LENGTH(bank)<5");

	if(!$insert_failed_txn){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_into_db = Qry($conn,"INSERT INTO rtgs_db(fno,type,com,amount,branch,crn,utr,timestamp) 
	SELECT fno,type,com,amount,branch,crn,bank,'$timestamp' FROM rtgs_fm WHERE timestamp_upload='$timestamp' AND CHAR_LENGTH(bank)>=5");

	if(!$insert_into_db){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_vou_no_adv = Qry($conn,"SELECT fno FROM rtgs_db WHERE timestamp='$timestamp' AND com='RRPL' AND type='ADVANCE'");

	if(!$get_vou_no_adv){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_vou_no_adv)>0)
	{
		while($row1 = fetchArray($get_vou_no_adv))
		{
			$frno = $row1['fno'];
				
			if(substr($frno,0,4)=='COAF' || substr($frno,0,4)=='DAHF' || substr($frno,0,4)=='MAGF')
			{
				$qry_update_coal = Qry($conn,"UPDATE ship.freight_memo_adv SET rtgs_adv=1 WHERE fm_no='$frno'");
				
				if(!$qry_update_coal){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
			else
			{
				$qry_update_coal = Qry($conn,"UPDATE freight_form SET rtgs_adv=1 WHERE frno='$frno'");
				
				if(!$qry_update_coal){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
		}
	}

	$get_vou_no_bal = Qry($conn,"SELECT fno FROM rtgs_db WHERE timestamp='$timestamp' AND com='RRPL' AND type='BALANCE'");

	if(!$get_vou_no_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_vou_no_bal)>0)
	{
		while($row2 = fetchArray($get_vou_no_bal))
		{
			$frno2 = $row2['fno'];
					
			if(substr($frno2,0,4)=='COAF' || substr($frno2,0,4)=='DAHF' || substr($frno2,0,4)=='MAGF')
			{
				$qry_update_coal = Qry($conn,"UPDATE ship.freight_memo_bal SET rtgs_bal=1 WHERE fm_no='$frno2'");
				
				if(!$qry_update_coal){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
			else
			{
				$qry_update_coal = Qry($conn,"UPDATE freight_form SET rtgs_bal=1 WHERE frno='$frno2'");
				
				if(!$qry_update_coal){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
		}
	}
	
	$copy_cache = Qry($conn,"INSERT INTO utr_sheet_cache_bak(crn,utr,utr_date,sheet_type,timestamp) 
	SELECT crn,utr,utr_date,'RRPL','$timestamp' FROM utr_sheet_cache WHERE timestamp='$timestamp'");

	if(!$copy_cache){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
				
	$dlt_cache = Qry($conn,"DELETE FROM utr_sheet_cache WHERE timestamp='$timestamp'");

	if(!$dlt_cache){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	echo '<form action="report.php" target="_blank" id="myForm" method="POST">
		<input type="hidden" name="timestamp" value="'.$timestamp.'">
	</form>';

	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		echo '<script>
			alert("SUCCESS : UTR SHEET UPLOADED SUCCESSFULLY !");
			$("#loadicon").fadeOut("slow");
			document.getElementById("myForm").submit();
		</script>';
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertRightCornerError("Error while processing request !");
		exit();
	}
}

?> 