<?php
require_once("connect.php");

$timestamp = date("Y:m:d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));

$update = Qry($conn,"UPDATE rtgs_fm SET approval='' WHERE id='$id' AND colset_d='' AND colset=''");
	
if(!$update)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

if(AffectedRows($conn) == 0)
{
	AlertRightCornerError("Something went wrong !");
	exit();
}

echo "<script>
	$('#checkbox_id_$id').attr('disabled',true);
	$('#edit_btn_$id').attr('disabled',true);
	$('#cancel_approval_btn_$id').attr('disabled',true);
	
	$('#edit_btn_$id').attr('onclick','');
	$('#cancel_approval_btn_$id').attr('onclick','');
	$('#loadicon').fadeOut('slow');
</script>";
?>