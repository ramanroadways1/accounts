<?php
require_once("connect.php");

$utr_date = escapeString($conn,strtoupper($_POST['utr_date']));
?>
<br />
	<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Company</th>
                        <th>Upload Timestamp</th>
                        <th>Record Uploaded</th>
                        <th>Successful Payment</th>
                        <th>Failed Payment</th>
                        <th>Download</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT sheet_type,timestamp FROM utr_sheet_cache_bak WHERE id in(SELECT MAX(id) FROM utr_sheet_cache_bak WHERE 
	date(timestamp)='$utr_date' GROUP BY timestamp)");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>
			<td colspan='7>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			$get_payment_count = Qry($conn,"SELECT id FROM utr_sheet_cache_bak WHERE timestamp='$row[timestamp]' AND sheet_type='$row[sheet_type]'");
			$get_payment_count_success = Qry($conn,"SELECT id FROM utr_sheet_cache_bak WHERE timestamp='$row[timestamp]' AND 
			sheet_type='$row[sheet_type]' AND utr!=''");
			$get_payment_count_failed = Qry($conn,"SELECT id FROM utr_sheet_cache_bak WHERE timestamp='$row[timestamp]' AND 
			sheet_type='$row[sheet_type]' AND utr=''");
			
			$total_count = numRows($get_payment_count);
			$total_count_success = numRows($get_payment_count_success);
			$total_count_failed = numRows($get_payment_count_failed);
			
			echo "<tr style='font-size:13px !important'>
				<td>$i</td>
				<td>$row[sheet_type]</td>
				<td>$timestamp</td>
				<td style='font-weight:bold;color:blue'>$total_count</td>
				<td style='font-weight:bold;color:green'><a style='font-weight:bold;color:green' href='./list_success_txn.php?company=$row[sheet_type]&timestamp=$row[timestamp]' target='_blank'>$total_count_success</a></td>
				<td style='font-weight:bold;color:red'><a style='font-weight:bold;color:red' href='./list_failed_txn.php?company=$row[sheet_type]&timestamp=$row[timestamp]' target='_blank'>$total_count_failed</a></td>
				<td>
					<form action='./download_utr_sheet.php' target='_blank' method='POST'>
						<input type='hidden' name='sheet_type' value='$row[sheet_type]'>
						<input type='hidden' name='timestamp' value='$row[timestamp]'>
						<button class='btn btn-xs btn-danger' type='submit'>Download</button>
					</form>
				</td>
			</tr>";
		$i++;	
		}
	}
	?>	
        </tbody>
    </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable({
		"lengthMenu": [ [10, 25, 100, -1], [10, 25, 100, "All"] ], 
	});
} );
</script> 			  