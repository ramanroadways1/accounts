<?php
require_once("connect.php");

$timestamp = date("Y:m:d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$ac_holder = escapeString($conn,strtoupper($_POST['ac_holder']));
$ac_no = escapeString($conn,strtoupper($_POST['ac_no']));
$bank = escapeString($conn,strtoupper($_POST['bank']));
$ifsc = escapeString($conn,strtoupper($_POST['ifsc']));

if(empty($id))
{
	AlertRightCornerError("Payment ID not found !");
	exit();
}

$check_rtgs = Qry($conn,"SELECT fno,acname,acno,bank_name,ifsc,type,colset_d,pan,tno FROM rtgs_fm WHERE id='$id'");

if(!$check_rtgs)
{
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}

if(numRows($check_rtgs) == 0)
{
	AlertRightCornerError("Payment not found !");
	exit();
}

$row_ac = fetchArray($check_rtgs);

$payment_type = $row_ac['type'];

if($payment_type!='ADVANCE' AND $payment_type!='BALANCE' AND $payment_type!='EXPENSE_VOU' AND $payment_type!='TRUCK_ADVANCE')
{
	AlertRightCornerError("Voucher type not found !");
	exit();
}

$old_ac_details = "Ac_Holder: $row_ac[acname], Ac_No: $row_ac[acno], Bank: $row_ac[bank_name], IFSC: $row_ac[ifsc]";

if($row_ac['acname']==$ac_holder AND $row_ac['acno']==$ac_no AND $row_ac['bank_name']==$bank AND $row_ac['ifsc']==$ifsc)
{
	AlertRightCornerError("Nothing to update !");
	echo "<script>$('#btn_submit_edit_ac').attr('disabled',false)</script>";
	exit();
}

$vou_no = $row_ac['fno'];

StartCommit($conn);
$flag = true;

if($payment_type=='ADVANCE' || $payment_type=='BALANCE')
{
	$get_fm = Qry($conn,"SELECT bid,oid,paidto,ptob FROM freight_form WHERE frno = '$vou_no'");
	
	if(!$get_fm){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_fm) == 0)
	{
		$flag = false;
		errorLog("Freight memo not found.",$conn,$page_name,__LINE__);
	}
	
	if($payment_type=='BALANCE')
	{
		$row_fm = fetchArray($get_fm);
		
		if($row_fm['paidto']=='BROKER')
		{
			$update_broker_ac = Qry($conn,"UPDATE mk_broker SET acname='$ac_holder',bank='$bank',accno='$ac_no',ifsc='$ifsc' WHERE id='$row_fm[bid]'");
			
			if(!$update_broker_ac){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn) == 0)
			{
				$flag = false;
				errorLog("Unable to update broker ac details. Broker_id: $row_fm[bid].",$conn,$page_name,__LINE__);
			}
		}
		else if($row_fm['paidto']=='OWNER')
		{
			$update_owner_ac = Qry($conn,"UPDATE mk_truck SET acname='$ac_holder',bank='$bank',accno='$ac_no',ifsc='$ifsc' WHERE id='$row_fm[oid]'");
			
			if(!$update_owner_ac){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn) == 0)
			{
				$flag = false;
				errorLog("Unable to update broker ac details. Broker_id: $row_fm[bid].",$conn,$page_name,__LINE__);
			}
		}
		else
		{
			$flag = false;
			errorLog("Balance to should be broker or owner. Vou_no: $vou_no.",$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$row_fm = fetchArray($get_fm);
		
		if($row_fm['ptob']=='BROKER')
		{
			$update_broker_ac = Qry($conn,"UPDATE mk_broker SET acname='$ac_holder',bank='$bank',accno='$ac_no',ifsc='$ifsc' WHERE id='$row_fm[bid]'");
			
			if(!$update_broker_ac){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn) == 0)
			{
				$flag = false;
				errorLog("Unable to update broker ac details. Broker_id: $row_fm[bid].",$conn,$page_name,__LINE__);
			}
		}
		else if($row_fm['ptob']=='OWNER')
		{
			$update_owner_ac = Qry($conn,"UPDATE mk_truck SET acname='$ac_holder',bank='$bank',accno='$ac_no',ifsc='$ifsc' WHERE id='$row_fm[oid]'");
			
			if(!$update_owner_ac){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn) == 0)
			{
				$flag = false;
				errorLog("Unable to update broker ac details. Broker_id: $row_fm[bid].",$conn,$page_name,__LINE__);
			}
		}
		else
		{
			$flag = false;
			errorLog("Advance to should be broker or owner. Vou_no: $vou_no.",$conn,$page_name,__LINE__);
		}
	}
}
else if($payment_type=='EXPENSE_VOU')
{
	$update_exp_ac = Qry($conn,"UPDATE exp_ac SET name='$ac_holder',acno='$ac_no',bank='$bank',ifsc='$ifsc' WHERE pan='$row_ac[pan]'");
		
	if(!$update_exp_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_exp_vou = Qry($conn,"UPDATE mk_venf SET neft_acname='$ac_holder',neft_acno='$ac_no',neft_bank='$bank',neft_ifsc='$ifsc' 
	WHERE vno='$vou_no'");
		
	if(!$update_exp_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_type=='TRUCK_ADVANCE')
{
	$get_truck_vou = Qry($conn,"SELECT dname,truckno,timestamp,user as branch,hisab_vou FROM mk_tdv WHERE tdvid='$vou_no'");
		
	if(!$get_truck_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_truck_vou) == 0)
	{
		$flag = false;
		errorLog("Truck voucher not found. Vou_no: $vou_no.",$conn,$page_name,__LINE__);
	}
	
	$row_tdv = fetchArray($get_truck_vou);
	
	if($row_tdv['hisab_vou']=="1")
	{
		$get_driver_code = Qry($conn,"SELECT driver_code FROM dairy.driver_book WHERE timestamp='$row_tdv[timestamp]' AND tno='$row_tdv[truckno]' 
		AND branch='$row_tdv[user]'");
		
		if(!$get_driver_code){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_driver_code) == 0)
		{
			$flag = false;
			errorLog("Driver code not found. Vou_no: $vou_no.",$conn,$page_name,__LINE__);
		}
		
		$row_driver_code = fetchArray($get_driver_code);
		
		$get_driver_name = Qry($conn,"SELECT name FROM dairy.driver WHERE code='$row_driver_code[driver_code]'");
		
		if(!$get_driver_name){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_driver_name) == 0)
		{
			$flag = false;
			errorLog("Driver code not found. Vou_no: $vou_no.",$conn,$page_name,__LINE__);
		}
		
		$row_driver_name = fetchArray($get_driver_name);
		
		if($row_driver_name['name'] != $row_tdv['dname'])
		{
			$flag = false;
			errorLog("Driver name not verified. Vou_no: $vou_no and driver_code: $row_driver_code[driver_code].",$conn,$page_name,__LINE__);
		}
		
		$update_driver_ac = Qry($conn,"UPDATE dairy.driver_ac SET acname='$ac_holder',acno='$ac_no',bank='$bank',ifsc='$ifsc' 
		WHERE code='$row_driver_code[driver_code]'");
		
		if(!$update_driver_ac){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$update_truck_vou = Qry($conn,"UPDATE mk_venf SET ac_name='$ac_holder',ac_no='$ac_no',bank='$bank',ifsc='$ifsc' 
	WHERE tdvid='$vou_no'");
		
	if(!$update_truck_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Invalid payment type.",$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO ac_update (o_b,ac_for,old_ac_details,acname,acno,bank,ifsc,timestamp) VALUES 
('$id','$payment_type','$old_ac_details','$ac_holder','$ac_no','$bank','$ifsc','$timestamp')");
		
if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_pending_payments = Qry($conn,"UPDATE rtgs_fm SET acname='$ac_holder',acno='$ac_no',bank_name='$bank',ifsc='$ifsc' WHERE id='$id' AND 
colset_d=''");
	
if(!$update_pending_payments){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn) == 0)
{
	$flag = false;
	errorLog("Unable to update ac details.",$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("OK : Updated !");	
	echo "<script>
		LoadTable();
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !");
	echo "<script>$('#btn_submit_edit_ac').attr('disabled',false)</script>";
	exit();
}
?>