<footer class="main-footer">
	<strong>Copyright &copy; <?php echo date("Y"); ?> Raman Group.</strong> All rights reserved.
</footer>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />     
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>	
  
<script src="../happay/bower_components/raphael/raphael.min.js"></script>
<script src="../happay/bower_components/morris.js/morris.min.js"></script>
<script src="../happay/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="../happay/bower_components/moment/min/moment.min.js"></script>
<script src="../happay/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="../happay/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../happay/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="../happay/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="../happay/bower_components/fastclick/lib/fastclick.js"></script>
<script src="../happay/dist/js/adminlte.min.js"></script>
<script src="../happay/dist/js/pages/dashboard.js"></script>
<script src="../happay/dist/js/demo.js"></script>
<script src="../happay/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../happay/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>

</body>
</html>

<script>
	$('#loadicon').fadeOut('slow');	  
</script>