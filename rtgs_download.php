<?php 
require_once './connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");

// $output = '';
$delimiter = ","; 

$com = escapeString($conn,$_POST['company']);

if($com!='RRPL' AND $com!='RAMAN_ROADWAYS')
{
	echo "<script>
		alert('Error: Something went wrong !');
		window.close();
	</script>";
	exit();
}

if($com=="RRPL")	
{
	$result = Qry($conn,"SELECT r.id,r.fno,r.acno,r.amount,r.bank_name,r.acname,r.crn,r.ifsc,r.pay_date,r.type,r.mobile,r.email,r.lrno,r.tno,r.com,
	r.branch,r.pan,f.from1,f.to1,u.email as email_id FROM rtgs_fm as r 
	LEFT OUTER JOIN freight_form as f ON f.frno=r.fno 
	LEFT OUTER JOIN user as u ON u.username=r.branch 
	WHERE r.colset_d!='1' AND r.com='RRPL' AND r.colset='1' AND r.approval='1' 
	AND r.fm_date>='2018-04-01' AND r.amount>0 ORDER BY r.crn ASC");

	if(!$result)
	{
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}

	if(numRows($result) == 0)
	{
		closeConnection($conn);
		echo "<script>
			alert('Nothing to download !');
			window.close();
		</script>";
		exit();
	}
	
	StartCommit($conn);
	$flag = true;
		 
	$f = fopen('php://memory', 'w'); 	 

	$fields = array('Id','Vou Type','Vou No','Transaction Type','Beneficiary  A/c No.','Transaction Amount','Beneficiary Name','Customer Ref No.',
	'IFSC Code','Beneficiary Mobile No.','Email ID','LR No','Truck No','Debit A/C Number','Branch','PAN No','BankName','FromStation','ToStation'); 
	fputcsv($f, $fields, $delimiter); 

	while($row = fetchArray($result))
	{
		  if(strpos($row['crn'],"RRPL-V") !== false)
		  {
				$ch = curl_init('http://vendor.rrpl.online/get_download_timestamp.php');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLINFO_HEADER_OUT, true);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("crn"=> $row['crn'],"timestamp"=>$timestamp)));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen(json_encode(array("crn"=> $row['crn'],"timestamp"=>$timestamp))))
				);
				
				$result_api = curl_exec($ch);
				
				if(curl_errno($ch))
				{
					$qry_log = Qry($conn,"INSERT INTO _api_vendor_log(crn,data,timestamp) VALUES ('$row[crn]','API_FAILED','$timestamp')");
					
					if(!$qry_log){
						$flag = false;
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					}
				}
				else
				{
					$output_api = json_decode($result_api,true);
					
					curl_close($ch);

					if($output_api['status']=='error')
					{
						$qry_log = Qry($conn,"INSERT INTO _api_vendor_log(crn,data,data2,timestamp) VALUES 
						('$row[crn]','API_ERROR','$output_api[errormsg]','$timestamp')");
						
						if(!$qry_log){
							$flag = false;
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						}
					}
					else
					{
						$qry_log = Qry($conn,"INSERT INTO _api_vendor_log(crn,data,data2,timestamp) VALUES 
						('$row[crn]','API_SUCCESS','$output_api[response]','$timestamp')");
						
						if(!$qry_log){
							$flag = false;
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						}
					}
				}
		  }
		  
		$lineData = array($row['id'],$row["type"],$row["fno"],'N',"'".$row['acno'], $row["amount"],$row['acname'], $row["crn"],$row["ifsc"],
		$row["mobile"],$row["email_id"],"'".$row['lrno'],$row["tno"],$row["com"],$row["branch"],$row["pan"],$row["bank_name"],$row["from1"],$row["to1"]); 
		fputcsv($f, $lineData, $delimiter); 
			
	  }
}
else if($com == "RAMAN_ROADWAYS")
{
	$result = Qry($conn,"SELECT r.id,r.fno,r.acno,r.amount,r.bank_name,r.acname,r.crn,r.ifsc,r.pay_date,r.type,r.mobile,r.email,r.lrno,r.tno,r.com,
	r.branch,r.pan,f.from1,f.to1,u.email as email_id FROM rtgs_fm as r 
	LEFT OUTER JOIN freight_form as f ON f.frno=r.fno 
	LEFT OUTER JOIN user as u ON u.username=r.branch
	WHERE r.colset_d!='1' AND r.com='RAMAN_ROADWAYS' AND r.colset='1' AND r.approval='1' 
	AND r.fm_date>='2018-04-01' AND r.amount>0 ORDER BY r.crn ASC");

	if(!$result)
	{
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($result) == 0)
	{
		closeConnection($conn);
		echo "<script>
			alert('Nothing to download !');
			window.close();
		</script>";
		exit();
	}
	
	StartCommit($conn);
	$flag = true;
	 
	$f = fopen('php://memory', 'w'); 	 

	$fields = array('Id','Vou Type','Vou No','Debit A/c Number','Beneficiary A/c Number','Beneficiary Name','Amount','Payment date','IFSC Code',
	'Beneficiary.Mobile No.','Beneficiary email-id','Lrno','LR No','Truck No','Ref. No','Branch','PAN No','BankName','FromStation','ToStation'); 
	fputcsv($f, $fields, $delimiter); 

	while($row = fetchArray($result))
	{
	  
	  if(strpos($row['crn'],"RR-V") !== false)
	  {
			$ch = curl_init('http://vendor.rrpl.online/get_download_timestamp.php');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("crn"=> $row['crn'],"timestamp"=>$timestamp)));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen(json_encode(array("crn"=> $row['crn'],"timestamp"=>$timestamp))))
			);
			$result_api = curl_exec($ch);
			if(curl_errno($ch))
			{
				$qry_log = Qry($conn,"INSERT INTO _api_vendor_log(crn,data,timestamp) VALUES ('$row[crn]','API_FAILED','$timestamp')");
			}
			else
			{
				$output_api = json_decode($result_api,true);
				curl_close($ch);

				if($output_api['status']=='error')
				{
					$qry_log = Qry($conn,"INSERT INTO _api_vendor_log(crn,data,data2,timestamp) VALUES 
					('$row[crn]','API_ERROR','$output_api[errormsg]','$timestamp')");
				}
				else
				{
					$qry_log = Qry($conn,"INSERT INTO _api_vendor_log(crn,data,data2,timestamp) VALUES 
					('$row[crn]','API_SUCCESS','$output_api[response]','$timestamp')");
				}
			}
	  }
	  
		$lineData = array($row['id'],$row["type"],$row["fno"],$row["com"],"'".$row['acno'],$row['acname'],$row["amount"],$row['pay_date'],$row["ifsc"],
		$row["mobile"],$row["email_id"],"'".$row['lrno'],$row["tno"],$row['crn'],$row["branch"],$row["pan"],$row["bank_name"],$row["from1"],$row["to1"]); 
		fputcsv($f, $lineData, $delimiter); 
	}
}
	
$copy_rtgs_done = Qry($conn,"INSERT INTO rtgs_done(fno,com,amount,branch,crn,timestamp) SELECT fno,com,amount,branch,crn,'$timestamp' 
FROM rtgs_fm WHERE colset_d!='1' AND com='$com' AND approval='1' AND fm_date>='2018-04-01' AND colset='1'");

if(!$copy_rtgs_done){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_timestamp = Qry($conn,"UPDATE rtgs_fm SET time_download='$timestamp' WHERE colset_d!='1' AND com='$com' AND colset='1' AND approval='1' 
AND fm_date>='2018-04-01'");

if(!$update_timestamp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_colset = Qry($conn,"UPDATE rtgs_fm SET colset_d='1' WHERE colset_d!='1' AND com='$com' AND colset='1' AND approval='1' AND 
fm_date>='2018-04-01'");

if(!$update_colset){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	fseek($f, 0); 
	header('Content-Type: text/csv'); 
	header('Content-Disposition: attachment; filename='.$com.'_NEFT_'.$timestamp.'.csv');
	fpassthru($f); 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error while processing request !');
		window.close();
	</script>";
	exit();
}
?>