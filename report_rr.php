<?php 
require_once './connect.php';

if(!isset($_POST['timestamp']))
{
	echo "<script>
		window.location.href='/';
	</script>";
	exit();
}

$timestamp = escapeString($conn,$_POST['timestamp']);

$qry = Qry($conn,"SELECT count(id) as success FROM rtgs_db WHERE timestamp='$timestamp'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row = fetchArray($qry);
$success = $row['success'];

$qry_u = Qry($conn,"SELECT count(id) as upload FROM rtgs_fm WHERE timestamp_upload='$timestamp'");

if(!$qry_u){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row2 = fetchArray($qry_u);
$uploaded = $row2['upload'];

$qry1 = Qry($conn,"SELECT count(id) as failed FROM rtgs_failed WHERE timestamp='$timestamp'");

if(!$qry1){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row1 = fetchArray($qry1);
$failed = $row1['failed'];

$qry_11 = Qry($conn,"SELECT count(id) as dupl FROM rtgs_db WHERE com='RAMAN_ROADWAYS' AND crn IN(SELECT crn FROM rtgs_db WHERE com='RAMAN_ROADWAYS' GROUP BY crn HAVING COUNT(crn)>1)");

if(!$qry_11){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row11 = fetchArray($qry_11);
$dupl = $row11['dupl'];

// $qry12 = Qry($conn,"SELECT count(id) as dupl2 FROM rtgs_failed WHERE com='RRPL' GROUP BY crn HAVING COUNT(*)>1");
// if(!$qry12){echo mysqli_error($conn);}
// $row12 = fetchArray($qry12);
// $dupl2 = $row12['dupl2'];
?>
<head>
<title>Report : UTR Sheet</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<body style="font-family:Verdana">

	<button style="margin:10px" onclick="window.close()" type="button" class="btn btn-sm btn-danger">Close window</button>
	
<div class="container">
<br />

<div class="row">
	<div class="col-md-12">
		<center><span style="font-size:18px">Report : UTR Sheet : RAMAN_ROADWAYS</span>
		<br />
		<br />
		<h4><font color="brown">Total Record Uploaded -</font> <?php echo $uploaded; ?></h4>
		<h4 style="margin-top:20px;"><a target="_blank" href="list_success_txn.php?timestamp=<?php echo $timestamp; ?>&company=RAMAN_ROADWAYS"><font color="green">Successful Payment -</font></a> <?php echo $success; ?></h4>
		<h4 style="margin-top:20px;"><a target="_blank" href="list_failed_txn.php?timestamp=<?php echo $timestamp; ?>&company=RAMAN_ROADWAYS"><font color="red">Payment Rejected -</font></a> <?php echo $failed; ?></h4>
		<h4 style="margin-top:20px;"><a target="_blank" href="list_duplicate_txn.php?timestamp=<?php echo $timestamp; ?>&company=RAMAN_ROADWAYS"><font color="red">Duplicate Payment -</font></a> <?php echo $dupl; ?></h4>
	</div>
	</center>
</div>

</div>